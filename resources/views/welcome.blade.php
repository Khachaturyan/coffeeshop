<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>
</head>
<body class="antialiased">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Проверка Адреса</h1>
        </div>
        <div class="col-md-12">
            <div>Начальный адрес: <span class="badge badge-secondary">г Москва, Проспект Вернадского, 105</span></div>
        </div>
        <div class="col-md-12">
            <form>
                <input type="hidden" value="Москва, Проспект Вернадского, 105" name="start_address" id="start_address">
                <div class="form-group">
                    <label for="address_select">Напишите конечный адрес: </label>
                    <select name="end_address" class="form-control" id="address_select">

                    </select>
                </div>
                <button type="submit" id="submit_button" disabled data-toggle="modal" data-target="#exampleModal"
                        class="btn btn-primary">Рассчитать растояние
                </button>
            </form>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ответ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>Начальный адрес: <span class="badge badge-secondary">г Москва, Проспект Вернадского, 105</span>
                </div>
                <div>Конечный адрес: <span class="badge badge-secondary" id="end_address"></span></div>
                <div>Растояние: <span class="badge badge-secondary" id="dist"></span></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $(document).ready(function () {
        $('#address_select').select2({
            ajax: {
                url: '{{route('autofill_address')}}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        address: params.term
                    }
                    return query;
                }
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            minimumInputLength: 3,
        }).on('select2:select', function (e) {
            $('#submit_button').prop('disabled', false);
        });

        $('#submit_button').on('click', function (e) {
            e.preventDefault();
            var start = $('#start_address').val();
            var end = $('#address_select').val();
            $('#end_address').html(end);
            $.ajax({
                type: "POST",
                url: '{{route('calculate')}}',
                data:
                    {
                        start_address: start,
                        end_address: end
                    }
            }).done(function (data) {
                $('#dist').html(data + ' км')
            });
        });
    });
</script>

</body>
</html>
