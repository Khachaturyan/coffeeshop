<?php

namespace App\Http\Controllers;

use Dadata\DadataClient;
use Illuminate\Http\Request;
use Location\Coordinate;
use Location\Distance\Vincenty;

class AddressController extends Controller
{


    /**
     * @var string
     */
    protected $token = '60e34d7e04059d6bfb0c8c442a206cfeb6da1ee9';
    /**
     * @var string
     */
    protected $secret = '611aaab5af646887e0646e74541343f221cffdea';


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAddress(Request $request)
    {
        $data = $request->all();
        $dadata = new DadataClient($this->token, $this->secret);
        $res = $dadata->suggest("address", $data['address']);

        $retAddresses = [];

        foreach ($res as $addressVal) {
            $retAddresses[] = [
                'id' => $addressVal['value'],
                'text' => $addressVal['value']
            ];
        }

        return response()->json(['results' => $retAddresses]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function calculate(Request $request)
    {

        $start_address = $request->start_address;
        $end_address = $request->end_address;

        $dadata = new DadataClient($this->token, $this->secret);

        $start = $dadata->clean("address", $start_address);
        $end = $dadata->clean("address", $end_address);


        $coordinate1 = new Coordinate($start['geo_lat'], $start['geo_lon']);
        $coordinate2 = new Coordinate(@$end['geo_lat'], @$end['geo_lon']);

        $calculator = new Vincenty();

        $res = $calculator->getDistance($coordinate1, $coordinate2);

        return response()->json(round($res / 1000, 2));
    }
}
