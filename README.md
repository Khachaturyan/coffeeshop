# Destination calculate service

## Installation
to install ... 

clone repository:

```shell 
$ git clone [url]
```

install the dependencies: 

```shell 
$ composer install
```

### Deployment

You may start Laravel's local development server using the Artisan CLI's serve command:
```shell 
$ php artisan serv
```



